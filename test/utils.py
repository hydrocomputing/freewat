# coding = utf-8

import os
from qgis.core import *
from qgis.gui import *

from PyQt4.QtGui import QDialogButtonBox

from ..mdoCreate_utils import createModel, createModelLayer
from ..dialogs.createGrid_dialog import QGridderDialog
from ..dialogs.createModelLayer_dialog import CreateModelLayerDialog

from ..logger import Logger

Logger.TESTING = True

class FakeIface(object):
    def __init__(self):
        self.__canvas = QgsMapCanvas()
    def activeLayer(self):
        return None
    def mapCanvas(self):
        return self.__canvas


def create_test_model(dbfile, app):
    
    if os.path.exists(dbfile):
        os.remove(dbfile)

    workdir, model = os.path.split(dbfile.replace('.sqlite',''))

    print 'model=',model, 'workdir=', workdir
    createModel(model, workdir, False, "m", "s", [365.0, 1, 1.0, 'SS'], '2016-01-01', '00:00:00', 'EPSG:2154')
    dlg = QGridderDialog(iface=FakeIface())
    dlg.textXmin.setText('0')
    dlg.textXmax.setText('100')
    dlg.textYmin.setText('0')
    dlg.textYmax.setText('100')
    dlg.textEdit.setText('grid')
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)

    app.processEvents()

    dlg = CreateModelLayerDialog(iface=None)
    dlg.txtLayerName.setText('layer1')
    dlg.txtTop.setText('10')
    dlg.txtBottom.setText('0')
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)

    app.processEvents()

