#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.QtGui import QIcon

from results.AKFeatureResultsSBD import AKFeatureResultsSBD
from AKFeatureCustomChartChem import AKFeatureCustomChartChem

class AKFeatureSBD (AKFeatureCustomChartChem):
    '''Generate an SBD plot with the selected measurements.'''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureSBD, self).__init__(settings, iface, windowTitle=u'SBD Measurements', parent=parent)
        self.m_icon = QIcon(settings.getIconPath('SBD.png'))
        self.m_settings = settings
        self.m_text = u'SBD Plot'
        self.customParametersOrder = ["N209MQL", "N436MQL", "N580MQL", "N247MQL", "N582MQL", "N168MQL"]
        self.m_plotSubFeature = AKFeatureResultsSBD(self.m_settings, self.iface, parent=self.m_measurementsSubFeature.m_form.window())
