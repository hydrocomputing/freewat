#
#coding=utf-8
#Copyright (C) 2015 Barcelona Science
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 Barcelona Science
:authors: L.M. de Vries, A. Nardi
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
import numpy as np
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from PyQt4 import QtGui
import sys
from collections import OrderedDict

from ChemPlot import ChemPlot
from ChemPlot import CPSETTINGS

class SARSETTINGS():
    sar = "sar"    
    polygon_color_serie_id = "polygon_color_serie"    
    default_settings = OrderedDict({
                    sar:{
                        polygon_color_serie_id:{
                            CPSETTINGS.value_key:"Blues",
                            CPSETTINGS.label_key:"Polygon color",
                            CPSETTINGS.type_value_key:"str"
                        }
                    }
                })

class SARPlot(ChemPlot):
    '''
    This class implements the SAR plot.
    '''
    def __init__(self, parent=None):
        super(SARPlot, self).__init__(parent)
        
        self.labels = []
        self.CE = []
        self.SAR = []
        
        self.legendPosition = 'timePlotLegend'
        
        self.m_settings.update(SARSETTINGS.default_settings)          
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_label_id, 'SAR Plot')
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_y_position_id, 1.05)
        self.set_setting(CPSETTINGS.window, CPSETTINGS.window_title_id, 'SAR')
        self.set_setting(CPSETTINGS.line_plot, CPSETTINGS.line_width_id, 0.0)               
        
    def setData(self, labels, values):
        self.labels = labels
        self.CE = values['CE']
        self.SAR = values['SAR']
        
    def pre_processing(self):
        patches = []
        
        coords = [[10,0],[10000,0],[10000,-8],[10,-8]]
        polygon = Polygon(coords, True)
        patches.append(polygon)
        
        coords = [[10,15],[10000,1],[10000,0],[10,0]]
        polygon = Polygon(coords, True)
        patches.append(polygon)
        
        coords = [[10,15],[10000,1],[10000,4],[10,25]]
        polygon = Polygon(coords, True)
        patches.append(polygon)
        
        coords = [[10,25],[10000,4],[10000,8],[10,35]]
        polygon = Polygon(coords, True)
        patches.append(polygon)
        
        coords = [[10,35],[10000,8],[10000,38],[10,38]]
        polygon = Polygon(coords, True)
        patches.append(polygon)
                
        pColors = np.array([0,1,2,3,4])
        serie_of_colors = self.setting(SARSETTINGS.sar, SARSETTINGS.polygon_color_serie_id)
        p = PatchCollection(patches, cmap=plt.get_cmap(serie_of_colors), alpha=0.3)
                
        p.set_array(np.array(pColors))
        self.ax.add_collection(p)        
        
        self.ax.grid()
        
        self.ax.axis([10, 10000, -8, 38])
        self.ax.set_xscale('log')
        plt.xlabel(r'Electric Conductivity ($\mu$s/cm)', fontsize=16)
        plt.ylabel(r'Sodium Adsorption Ratio (SAR)', fontsize=16)
        
        self.ax.xaxis.set_tick_params(size=10)
        self.ax.yaxis.set_tick_params(size=10)
        self.ax.tick_params(axis='both', which='major', labelsize=15)
        self.ax.tick_params(axis='both', which='minor', labelsize=12)
        
        self.ax.text(106, -4, "low(1)", va='center', ha='left')
        self.ax.text(196, -4, "medium(2)", va='center', ha='left')
        self.ax.text(856, -4, "high(3)", va='center', ha='left')
        self.ax.text(2206, -4, "very high(4)", va='center', ha='left')
        
        self.ax.text(12, 5, "low(1)", va='center', ha='left')
        self.ax.text(12, 18, "medium(2)", va='center', ha='left')
        self.ax.text(12, 28, "high(3)", va='center', ha='left')
        self.ax.text(12, 36, "very high(4)", va='center', ha='left')
        
        plt.axvline(x=100, c="k")
        plt.axvline(x=180, c="k")
        plt.axvline(x=800, c="k")
        plt.axvline(x=2000, c="k")  
    
    def plot_values(self):
        
        for curPoint in range(len(self.labels)): 
            x = self.CE[curPoint]
            y = self.SAR[curPoint]    
            label = self.labels[curPoint]                       
            self.private_plot(x, y, label, curPoint)

def plotSar1():
        
    myPlot = SARPlot()    
    labels = ["Well_1", "Well_2", "Well_3", "Well_4", "Well_5", "Well_6", "Well_7", "Well_8", "Well_9", "Well_10", "Well_11", "Well_12" , "Well_13"]    
    SAR = [1.5, 4.5, 2.5, 3.5, 1.5, 4.5, 2.5, 3.5, 1.5, 4.5, 2.5, 3.5, 1.5, 4.5, 2.5, 3.5 , 1.5]
    CE = [1200, 1500, 3200, 1300, 1340, 1550, 4200, 1400, 1340, 1200, 2200, 5300, 3340, 2550, 1200, 2400, 2430]
    values = {'SAR':SAR, 'CE':CE}  
    myPlot.setData(labels, values)
    myPlot.draw()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)    
    plotSar1()
    sys.exit(app.exec_())