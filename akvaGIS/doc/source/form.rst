form package
============

Submodules
----------

form.AKForm module
------------------

.. automodule:: form.AKForm
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormCreateDB module
--------------------------

.. automodule:: form.AKFormCreateDB
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormDBManagement module
------------------------------

.. automodule:: form.AKFormDBManagement
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormHydroUnitSelection module
------------------------------------

.. automodule:: form.AKFormHydroUnitSelection
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormNormative module
---------------------------

.. automodule:: form.AKFormNormative
    :members:
    :undoc-members:
    :show-inheritance:


form.AKFormQueryMeasurements module
-----------------------------------

.. automodule:: form.AKFormQueryMeasurements
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormQuerySamples module
------------------------------

.. automodule:: form.AKFormQuerySamples
    :members:
    :undoc-members:
    :show-inheritance:

form.AKFormSamplesResults module
--------------------------------

.. automodule:: form.AKFormSamplesResults
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: form
    :members:
    :undoc-members:
    :show-inheritance:
