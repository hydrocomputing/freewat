AkvaGIS Packages and Modules
============================

.. toctree::
   :maxdepth: 4

   AKSettings
   feature
   form
   model
   resources
   utils
   view
