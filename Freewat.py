# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Freewat
                                 A QGIS plugin
 Build and Run MODFLOW models
                              -------------------
        begin                : 2015-01-06
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Iacopo Borsi TEA Sistemi
        email                : iacopo.borsi@tea-group.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from install import installer as inst

inst.check_install()

from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QFileInfo
from PyQt4.QtGui import QAction, QIcon, QMenu
from qgis.core import *
# Import the code for the dialog
from dialogs import createModelLayer_dialog as mlDialog
from dialogs import createModel_dialog as mdDialog
from dialogs import programLocations_dialog as plDialog
from dialogs import createCHDLayer_dialog as chdDialog
from dialogs import createWELLayer_dialog as welDialog
from dialogs import createMNWLayer_dialog as mnwDialog
from dialogs import createRCHLayer_dialog as rchDialog
from dialogs import createRIVLayer_dialog as rivDialog
from dialogs import createLAKLayer_dialog as lakDialog
from dialogs import createDRNLayer_dialog as drnDialog
from dialogs import createGHBLayer_dialog as ghbDialog
from dialogs import createEVTLayer_dialog as evtDialog
from dialogs import createUZFLayer_dialog as uzfDialog
from dialogs import createSMLLayer_dialog as smlDialog
from dialogs import createSFRLayer_dialog as sfrDialog
from dialogs import createTransportModel_dialog as ttableDialog
from dialogs import createTRNSPLayer_dialog as trnspDialog
from dialogs import createSSMLayer_dialog as ssmDialog
from dialogs import createUZTLayer_dialog as uztDialog
from dialogs import createSFTLayer_dialog as sftDialog
from dialogs import createLKTLayer_dialog as lktDialog
from dialogs import createReactionLayer_dialog as rctDialog
from dialogs import createModelBuilder_dialog as mbDialog
from dialogs import viewOutput_dialog as outDialog
from dialogs import viewBudget_dialog as budgetDialog
from dialogs import runZoneBudget_dialog as zbudDialog
from dialogs import updateWd as updateWd
from dialogs import viewCrossSection_dialog as csDialog
from dialogs import createTMRfile_dialog as tmrDialog
from dialogs import createInputFilesForOBS as OBSfiles

# TO DO merge this plot into the main Calibration dialog
import SimplePlotCalibration as scpDialog

# Temporary solution to manage some dependencies issue:
runPlotLoaded = True
try:
    import runPlotCalibration_dialog as pcDialog
except Exception as e:
    print e
    runPlotLoaded = False
#
from dialogs import createPartikelTracking_dialog as ptDialog
from dialogs import createZoneLayer_dialog as zoneDialog
from dialogs import addStressPeriod_dialog as spDialog
from dialogs import copyFromVector_dialog as copyVectorDialog
from dialogs import copyRasterToFields_dialog as copyRasterDialog
from dialogs import mergeTools_dialog as mergeDialog
from dialogs import createGrid_dialog as grDialog
from dialogs import createFarmCrop_dialog as cropDialog
from dialogs import createFarmId_dialog as fmpidDialog
from dialogs import createFarmWells_dialog as fmpwellDialog
from dialogs import createWaterSitesTable_dialog as watersitesDialog
from dialogs import loadGWAllotments_dialog as gwallotDialog
from dialogs import loadSWAllotments_dialog as swallotDialog
from dialogs import loadNRDeliveries_dialog as nrdelivDialog
from dialogs import createCropTable_dialog as croptableDialog
from dialogs import createSoilsTable_dialog as soilstableDialog
from dialogs import createFarmPipeline_dialog as fmpipeDialog
from dialogs import createHOBLayer_dialog as hobDialog
from dialogs import createRVOBLayer_dialog as rvobDialog
from dialogs import createParams3dArray_dialog as param3DDialog
from dialogs import createParams2dArray_dialog as param2DDialog
from dialogs import loadClimateData_dialog as climateDialog
from dialogs import crop_dialog as CropModuleDialog
from dialogs import crop_run as CropRunDialog
from dialogs import loadCropCoefficients_dialog as cropCoefficientDialog
from dialogs import createUSBLayer_dialog as usbDialog
from dialogs import runUSBcalculus_dialog as runUsbDialog
from dialogs import plotWaterUnitBudget_dialog as fmpPlotDialog
from dialogs import aboutdialog as aboutDialog
from dialogs import importModflowModel_dialog as importModflowDialog
from dialogs import createSWI2Layer_dialog as swiDialog
from dialogs import viewSWIOutput_dialog as viewSWIDialog
import os.path
import webbrowser
# akvaGIS
from akvaGIS.AKSettings import AKSettings
from akvaGIS import feature as feature
#
import sys
import os

from osgeo.ogr import Geometry
import traceback
oatLoaded = True
try:
    # add oat module to PYTHONPATH
    sys.path.append(os.path.dirname(__file__))
    from oat import oatInit
except:
    oatLoaded = False
    pass

# --
class Freewat:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # temporary solution (iac):
        if not runPlotLoaded:
            from PyQt4.Qt import QMessageBox
            messagePlot  = ''' To run tools for plotting Calibration Results you need that
            the following Python module is installed:
            Seaborn
            Please, check your installation!

            This part will not be loaded, until you have fixed
            this dependency issue,
            but you can use FREEWAT anyway ... '''
            QMessageBox.information(self.iface.mainWindow(), "Warning", messagePlot)

        # ----- Needed by akvaGIS

        sys.path.append(os.path.normpath((os.path.dirname(os.path.realpath(__file__)) + "/akvaGIS")))
        sys.path.append(os.path.normpath((os.path.dirname(os.path.realpath(__file__)) + "/akvaGIS/external")))
        sys.path.append(os.path.normpath((os.path.dirname(os.path.realpath(__file__)) + "/akvaGIS/feature")))

        # Check that all dependencies for akvaGIS are satisfied
        self.allModulesLoaded = True
        from PyQt4.Qt import QMessageBox
        try:
            from PyQt4.Qt import QSqlDatabase
        except ImportError:
            self.allModulesLoaded = False
            QMessageBox.information(iface.mainWindow(),
                                "Warning",
                                "Make sure that the python PyQt Sql package is installed.")
        try:
            import xlrd
        except ImportError:
            self.allModulesLoaded = False
            QMessageBox.warning(iface.mainWindow(),
                                "Warning",
                                "Make sure that the python xlrd package is installed.")
        try:
            import xlwt
        except ImportError:
            self.allModulesLoaded = False
            QMessageBox.warning(iface.mainWindow(),
                                "Warning",
                                "Make sure that the python xlwt package is installed.")

        # Import akvaGIS objects

        from feature.AKFeatureCreateDB import AKFeatureCreateDB
        from feature.AKFeatureCloseDB import AKFeatureCloseDB
        from feature.AKFeatureDBManagement import AKFeatureDBManagement

        from feature.AKFeaturePiper import AKFeaturePiper
        from feature.AKFeatureSAR import AKFeatureSAR
        from feature.AKFeatureSBD import AKFeatureSBD
        from feature.AKFeatureStiffPlot import AKFeatureStiffPlot
        from feature.AKFeatureIonicBalance import AKFeatureIonicBalance

        from feature.AKFeatureChemTimePlot import AKFeatureChemTimePlot
        from feature.AKFeatureHydroTimePlot import AKFeatureHydroTimePlot
        from feature.AKFeatureMapChem import AKFeatureMapChem
        from feature.AKFeatureMapNormative import AKFeatureMapNormative

        from feature.AKFeatureMapStiff import AKFeatureMapStiff

        from feature.AKFeatureStatQuimet import AKFeatureStatQuimet
        from feature.AKFeatureEasyQuim import AKFeatureEasyQuim
        from feature.AKFeatureExcelMix import AKFeatureExcelMix

        from feature.AKFeatureSamplesChemistry import AKFeatureSamplesChemistry
        from feature.AKFeatureSamplesHydrogeology import AKFeatureSamplesHydrogeology
        from feature.AKFeatureMapHydro import AKFeatureMapHydro
        from feature.AKFeatureMapHydroSurface import AKFeatureMapHydroSurface

        # -- akvaGIS until here


        # For i18n support
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'freewat_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)




        # Declare instance attributes
        self.actions = []

        # needed by akvaGIS
        # QUery and DB management group
        self.m_AKSettings = AKSettings(iface)
        self.m_CreateDB = AKFeatureCreateDB(self.m_AKSettings, iface, "create", self.actions)
        self.m_OpenDB = AKFeatureCreateDB(self.m_AKSettings, iface, "open", self.actions)
        self.m_CloseDB = AKFeatureCloseDB(self.m_AKSettings, iface, "close", self.actions)
        self.m_ManageDB = AKFeatureDBManagement(self.m_AKSettings, iface)

        self.m_QuerySamples = AKFeatureSamplesChemistry(self.m_AKSettings, iface)

        # Plots
        self.m_Piper = AKFeaturePiper(self.m_AKSettings, iface)
        self.m_SBD = AKFeatureSBD(self.m_AKSettings, iface)
        self.m_SAR = AKFeatureSAR(self.m_AKSettings, iface)
        self.m_stiffPlot = AKFeatureStiffPlot(self.m_AKSettings, iface)
        self.m_chemTimePlot = AKFeatureChemTimePlot(self.m_AKSettings, iface)
        self.m_IBalance = AKFeatureIonicBalance(self.m_AKSettings, iface)

        # Maps
        self.m_StiffMap = AKFeatureMapStiff(self.m_AKSettings, iface)
        self.m_Map = AKFeatureMapChem(self.m_AKSettings, iface)
        self.m_MapNormative = AKFeatureMapNormative(self.m_AKSettings, iface)

        # Excel tools
        self.m_EasyQuim = AKFeatureEasyQuim(self.m_AKSettings, iface)
        self.m_ExcelMix = AKFeatureExcelMix(self.m_AKSettings, iface)
        self.m_StatQuimet = AKFeatureStatQuimet(self.m_AKSettings, iface)

        # Hydro tools
        self.m_HydroSamples = AKFeatureSamplesHydrogeology(self.m_AKSettings, iface)
        self.m_hydroTimePlot = AKFeatureHydroTimePlot(self.m_AKSettings, iface)
        self.m_HydroMap = AKFeatureMapHydro(self.m_AKSettings, iface)
        self.m_HydroMapSurface = AKFeatureMapHydroSurface(self.m_AKSettings, iface)

##        # Declare instance attributes
##        self.menu = u'&akvaGIS'
##        # TODO: We are going to let the user set this up in a future iteration
##        self.toolbar = self.iface.addToolBar(u'akvaGIS')
##        self.toolbar.setObjectName(u'akvaGIS')

        # --- for akvaGIS until here


        self.menu = self.tr(u'Freewat')
        #
        self.toolbar = self.iface.addToolBar(u'Freewat')
        self.toolbar.setObjectName(u'Freewat')

    def getIconPath(self, filename):
        """returns complete path of icon"""
        return os.path.join(os.path.dirname(__file__), 'icons', filename)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Freewat', message)


    # needed by akvaGIS - -- modified after inserting within FREEWAT Plugin
    #                        insert of submenu attribute
    def add_feature(
        self,
        feature=None,
        parent=None,
        enabled=False, submenu = None):
        """        """

        icon = feature.m_icon
        text = feature.m_text
        action = QAction(icon, text, parent)
        action.triggered.connect(feature.run)
        action.setEnabled(enabled)

        status_tip = "status_tip"
        action.setStatusTip(status_tip)
        whats_this = "whats_this"
        action.setWhatsThis(whats_this)

        if True:
            submenu.addAction(action)

##        if True:
##            self.iface.addPluginToMenu(
##                self.menu,
##                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        self.menu = QMenu(self.iface.mainWindow())
        self.menu.setTitle("&FREEWAT")

        # Model Setup

        # Create Model
        self.actionCreateModel = QAction(QIcon(self.getIconPath("model.png")), self.tr("Create Model"), self.iface.mainWindow())
        self.actionCreateModel.setWhatsThis("Define a new Model")
        self.actionCreateModel.setStatusTip("This is status tip")
        self.actionCreateModel.triggered.connect(self.runCreateModel)

        # Import Modflow Model
        self.actionModflowModel = QAction(QIcon(self.getIconPath("model.png")), self.tr("Import a MODFLOW Model"), self.iface.mainWindow())
        self.actionModflowModel.setWhatsThis("Import a NAM file")
        self.actionModflowModel.setStatusTip("This is status tip")
        self.actionModflowModel.triggered.connect(self.runImportModflow)

        # Program Locations
        self.actionProgramLocations = QAction( self.tr("&Program &Locations"), self.iface.mainWindow())
        self.actionProgramLocations.setWhatsThis("Configuration for test plugin")
        self.actionProgramLocations.setStatusTip("This is status tip")
        self.actionProgramLocations.triggered.connect(self.runProgramLocations)

        # Create Grid
        self.actionCreateGrid = QAction(QIcon(self.getIconPath("grid.png")), self.tr("Create Grid"), self.iface.mainWindow())
        self.actionCreateGrid.setWhatsThis("Configuration for test plugin")
        self.actionCreateGrid.setStatusTip("This is status tip")
        self.actionCreateGrid.triggered.connect(self.runCreateGrid)

        # Set Telescopic Grid Refinement file
        self.actionSetTMR  = QAction(self.tr("Set File for TMR Grid Refinement"), self.iface.mainWindow())
        self.actionSetTMR.setWhatsThis("Configuration for test plugin")
        self.actionSetTMR.setStatusTip("This is status tip")
        self.actionSetTMR.triggered.connect(self.runTMR)

        # Create Model Layer
        self.actionCreateModelLayer = QAction( self.tr("Create Model Layer"), self.iface.mainWindow())
        self.actionCreateModelLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateModelLayer.setStatusTip("This is status tip")
        self.actionCreateModelLayer.triggered.connect(self.runCreateModelLayer)

        # Create CHD Layer
        self.actionCreateCHDLayer = QAction( self.tr("Create CHD Layer"), self.iface.mainWindow())
        self.actionCreateCHDLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateCHDLayer.setStatusTip("This is status tip")
        self.actionCreateCHDLayer.triggered.connect(self.runCreateCHD)

        # Create WEL Layer
        self.actionCreateWELlayer = QAction(self.tr("Create WEL Layer"), self.iface.mainWindow())
        self.actionCreateWELlayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateWELlayer.setStatusTip("This is status tip")
        self.actionCreateWELlayer.triggered.connect(self.runCreateWEL)

        # Create MNW Layer
        self.actionCreateMNWlayer = QAction(self.tr("Create MNW Layer"), self.iface.mainWindow())
        self.actionCreateMNWlayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateMNWlayer.setStatusTip("This is status tip")
        self.actionCreateMNWlayer.triggered.connect(self.runCreateMNW)

        # Create RCH Layer
        self.actionCreateRCHLayer= QAction( self.tr("Create RCH Layer"), self.iface.mainWindow())
        self.actionCreateRCHLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateRCHLayer.setStatusTip("This is status tip")
        self.actionCreateRCHLayer.triggered.connect(self.runCreateRCH)

        # Create RIV Layer
        self.actionCreateRIVLayer= QAction(self.tr("Create RIV Layer"), self.iface.mainWindow())
        self.actionCreateRIVLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateRIVLayer.setStatusTip("This is status tip")
        self.actionCreateRIVLayer.triggered.connect(self.runCreateRIV)

        # Create LAK Layer
        self.actionCreateLAKLayer = QAction("Create LAK Layer", self.iface.mainWindow())
        self.actionCreateLAKLayer.triggered.connect(self.runCreateLAK)

        # Create DRN Layer
        self.actionCreateDRNLayer= QAction(self.tr("Create DRN Layer"), self.iface.mainWindow())
        self.actionCreateDRNLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateDRNLayer.setStatusTip("This is status tip")
        self.actionCreateDRNLayer.triggered.connect(self.runCreateDRN)

        # Create GHB Layer
        self.actionCreateGHBLayer= QAction(self.tr("Create GHB Layer"), self.iface.mainWindow())
        self.actionCreateGHBLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateGHBLayer.setStatusTip("This is status tip")
        self.actionCreateGHBLayer.triggered.connect(self.runCreateGHB)

        # Create EVT Layer
        self.actionCreateEVTLayer= QAction(self.tr("Create EVT Layer"), self.iface.mainWindow())
        self.actionCreateEVTLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateEVTLayer.setStatusTip("This is status tip")
        self.actionCreateEVTLayer.triggered.connect(self.runCreateEVT)

        # Create UZF Layer
        self.actionCreateUZFLayer= QAction(self.tr("Create UZF Layer"), self.iface.mainWindow())
        self.actionCreateUZFLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateUZFLayer.setStatusTip("This is status tip")
        self.actionCreateUZFLayer.triggered.connect(self.runCreateUZF)

        # Create SFR Layer
        self.actionCreateSFRLayer= QAction(self.tr("Create SFR Layer"), self.iface.mainWindow())
        self.actionCreateSFRLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSFRLayer.setStatusTip("This is status tip")
        self.actionCreateSFRLayer.triggered.connect(self.runCreateSFR)

        # Create SML Layer
        self.actionCreateSMLLayer= QAction(self.tr("Create Surface Model Layer"), self.iface.mainWindow())
        self.actionCreateSMLLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSMLLayer.setStatusTip("This is status tip")
        self.actionCreateSMLLayer.triggered.connect(self.runCreateSML)

        # Create SML Layer for Farm (just change the name)
        #self.actionCreateSMLLayer= QAction(QIcon(":/plugins/Freewat/icons/icon.png"), "Create SML Layer", self.iface.mainWindow())
        self.actionCreateSMLFarmLayer= QAction(self.tr("Create Ref. Evapotransp. Layer"), self.iface.mainWindow())
        self.actionCreateSMLFarmLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSMLFarmLayer.setStatusTip("This is status tip")
        self.actionCreateSMLFarmLayer.triggered.connect(self.runCreateSML)

        # Create Zone Layer
        self.actionCreateZoneLayer= QAction(self.tr("Create Zones Layer"), self.iface.mainWindow())
        self.actionCreateZoneLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateZoneLayer.setStatusTip("This is status tip")
        self.actionCreateZoneLayer.triggered.connect(self.runCreateZone)

        # Create Zone Layer
        self.actionCreateSWILayer= QAction(self.tr("Create SWI Layer"), self.iface.mainWindow())
        self.actionCreateSWILayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSWILayer.setStatusTip("This is status tip")
        self.actionCreateSWILayer.triggered.connect(self.runCreateSWI)

        # Create Transport Table
        self.actionCreateTrTableLayer= QAction(self.tr("Create &Transport Model"), self.iface.mainWindow())
        self.actionCreateTrTableLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateTrTableLayer.setStatusTip("This is status tip")
        self.actionCreateTrTableLayer.triggered.connect(self.runCreateTrTable)

        # Create Transport Layer
        self.actionCreateTRNSPLayer= QAction(self.tr("Create &Transport Layer(s)"), self.iface.mainWindow())
        self.actionCreateTRNSPLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateTRNSPLayer.setStatusTip("This is status tip")
        self.actionCreateTRNSPLayer.triggered.connect(self.runCreateTRNSP)


        # Create Sink and Source Layer
        self.actionCreateSSMLayer= QAction(self.tr("Create Sink and Source Layer"), self.iface.mainWindow())
        self.actionCreateSSMLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSSMLayer.setStatusTip("This is status tip")
        self.actionCreateSSMLayer.triggered.connect(self.runCreateSSM)


        # Create Reaction Layer
        self.actionCreateRCTLayer= QAction(self.tr("Create Reaction Layer"), self.iface.mainWindow())
        self.actionCreateRCTLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateRCTLayer.setStatusTip("This is status tip")
        self.actionCreateRCTLayer.triggered.connect(self.runCreateRCT)

        # Create Unsaturated Transport Layer
        self.actionCreateUZTLayer= QAction(self.tr("Create Unsaturated Zone Layer"), self.iface.mainWindow())
        self.actionCreateUZTLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateUZTLayer.setStatusTip("This is status tip")
        self.actionCreateUZTLayer.triggered.connect(self.runCreateUZT)

        # Create StreamFlow Transport Layer
        self.actionCreateSFTLayer= QAction(self.tr("Create StreamFlow Transport Layer"), self.iface.mainWindow())
        self.actionCreateSFTLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateSFTLayer.setStatusTip("This is status tip")
        self.actionCreateSFTLayer.triggered.connect(self.runCreateSFT)

        # Create Lake Transport Tables
        self.actionCreateLKTLayer= QAction(self.tr("Create Lake Transport Tables"), self.iface.mainWindow())
        self.actionCreateLKTLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateLKTLayer.setStatusTip("This is status tip")
        self.actionCreateLKTLayer.triggered.connect(self.runCreateLKT)

        # Create USB Layer
        self.actionCreateUSBLayer= QAction(self.tr("Create Unsat Solute Balance Layer"), self.iface.mainWindow())
        self.actionCreateUSBLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateUSBLayer.setStatusTip("This is status tip")
        self.actionCreateUSBLayer.triggered.connect(self.runCreateUSB)

        # Run  USB  Calculus
        self.actionRunUSB = QAction(self.tr("Run Unsat Solute Balance Calculus"), self.iface.mainWindow())
        self.actionRunUSB.setWhatsThis("Configuration for test plugin")
        self.actionRunUSB.setStatusTip("This is status tip")
        self.actionRunUSB.triggered.connect(self.runUSB)

        # WATER management and Agriculture
        # Define  Farm_ID
        self.actionCreateFarmID = QAction(QIcon(self.getIconPath("water.png")), self.tr("Define Water Demand Units (Farm_ID)"), self.iface.mainWindow())
        self.actionCreateFarmID.setWhatsThis("Configuration for test plugin")
        self.actionCreateFarmID.setStatusTip("This is status tip")
        self.actionCreateFarmID.triggered.connect(self.runCreateFarmID)

        # Insert Water Units Parameters
        self.actionCreateWaterUnits = QAction(self.tr("Insert Water Units Properties"), self.iface.mainWindow())
        self.actionCreateWaterUnits.setWhatsThis("Configuration for test plugin")
        self.actionCreateWaterUnits.setStatusTip("This is status tip")
        self.actionCreateWaterUnits.triggered.connect(self.runDefineWaterUnits)

        # Load Non-routed deliveries
        self.actionLoadNRD = QAction(self.tr("Load External Deliveries (Non-routed Deliv.)"), self.iface.mainWindow())
        self.actionLoadNRD.setWhatsThis("Configuration for test plugin")
        self.actionLoadNRD.setStatusTip("This is status tip")
        self.actionLoadNRD.triggered.connect(self.runLoadNRDeliv)

        # Load GW Allotments
        self.actionLoadGWAllotments = QAction(self.tr("Load GW Rights (GW Allotments)"), self.iface.mainWindow())
        self.actionLoadGWAllotments.setWhatsThis("Configuration for test plugin")
        self.actionLoadGWAllotments.setStatusTip("This is status tip")
        self.actionLoadGWAllotments.triggered.connect(self.runLoadGWAlots)

        # Load SW Allotments
        self.actionLoadSWAllotments = QAction(self.tr("Load SW Rights (SW Allotments)"), self.iface.mainWindow())
        self.actionLoadSWAllotments.setWhatsThis("Configuration for test plugin")
        self.actionLoadSWAllotments.setStatusTip("This is status tip")
        self.actionLoadSWAllotments.triggered.connect(self.runLoadSWAlots)

        # Define Soils Table
        self.actionCreateSoilsTable = QAction(self.tr("Define Soils properties"), self.iface.mainWindow())
        self.actionCreateSoilsTable.setWhatsThis("Configuration for test plugin")
        self.actionCreateSoilsTable.setStatusTip("This is status tip")
        self.actionCreateSoilsTable.triggered.connect(self.runCreateSoilsTable)

        # Define Farm Wells
        self.actionCreateFarmWells = QAction(self.tr("Define Wells for a Water Unit (Farm Well)"), self.iface.mainWindow())
        self.actionCreateFarmWells.setWhatsThis("Configuration for test plugin")
        self.actionCreateFarmWells.setStatusTip("This is status tip")
        self.actionCreateFarmWells.triggered.connect(self.runCreateFarmWells)

        # Define Farm Pipeline
        self.actionCreateFarmPipeline = QAction(self.tr("Define Pipelines for a Water Unit"), self.iface.mainWindow())
        self.actionCreateFarmPipeline.setWhatsThis("Configuration for test plugin")
        self.actionCreateFarmPipeline.setStatusTip("This is status tip")
        self.actionCreateFarmPipeline.triggered.connect(self.runCreateFarmPipeline)

        # Define Farm Crops
        self.actionCreateFarmCrops = QAction(self.tr("Define Crops and Soils for a Water Unit"), self.iface.mainWindow())
        self.actionCreateFarmCrops.setWhatsThis("Configuration for test plugin")
        self.actionCreateFarmCrops.setStatusTip("This is status tip")
        self.actionCreateFarmCrops.triggered.connect(self.runCreateFarmCrop)

        # Define Crops Table
        self.actionCreateCropTable = QAction(self.tr("Define Crops properties"), self.iface.mainWindow())
        self.actionCreateCropTable.setWhatsThis("Configuration for test plugin")
        self.actionCreateCropTable.setStatusTip("This is status tip")
        self.actionCreateCropTable.triggered.connect(self.runCreateCropTable)

        # Define Crop Coeffients for FARM PROCESS
        self.actionCropCoeffients = QAction(self.tr("Load Root and Crop Coefficients"), self.iface.mainWindow())
        self.actionCropCoeffients.setWhatsThis("Configuration for test plugin")
        self.actionCropCoeffients.setStatusTip("This is status tip")
        self.actionCropCoeffients.triggered.connect(self.runLoadCropCoeffients)

        # Define Climate Data for FARM PROCESS
        self.actionClimateData = QAction(self.tr("Load Climate Data"), self.iface.mainWindow())
        self.actionClimateData.setWhatsThis("Configuration for test plugin")
        self.actionClimateData.setStatusTip("This is status tip")
        self.actionClimateData.triggered.connect(self.runLoadClimateData)

        # Define Crop Module Layer for FARM PROCESS
        self.actionCropModule = QAction(self.tr("Create Crop Module Layer"), self.iface.mainWindow())
        self.actionCropModule.setWhatsThis("Configuration for test plugin")
        self.actionCropModule.setStatusTip("This is status tip")
        self.actionCropModule.triggered.connect(self.runCropModule)

        # Define Run Crop Model for FARM PROCESS
        self.actionCropModel = QAction(self.tr("Run Crop Model"), self.iface.mainWindow())
        self.actionCropModel.setWhatsThis("Configuration for test plugin")
        self.actionCropModel.setStatusTip("This is status tip")
        self.actionCropModel.triggered.connect(self.runCropModel)

        # Observations and Calibration

        # Create input files for Observations
        self.actionCreateInputFilesForOBS = QAction(self.tr("Create input files for Observations"), self.iface.mainWindow())
        self.actionCreateInputFilesForOBS.setWhatsThis("Configuration for test plugin")
        self.actionCreateInputFilesForOBS.setStatusTip("This is status tip")
        self.actionCreateInputFilesForOBS.triggered.connect(self.runCreateInputFilesForOBS)

        # HOB
        # Create HOB Layer
        self.actionCreateHOBLayer = QAction(self.tr("Create Head Observations Layer"), self.iface.mainWindow())
        self.actionCreateHOBLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateHOBLayer.setStatusTip("This is status tip")
        self.actionCreateHOBLayer.triggered.connect(self.runCreateHOB )

        # Create Flow OBS Layer
        self.actionCreateRVOBLayer = QAction(self.tr("Create Flow Observations Layer"), self.iface.mainWindow())
        self.actionCreateRVOBLayer.setWhatsThis("Configuration for test plugin")
        self.actionCreateRVOBLayer.setStatusTip("This is status tip")
        self.actionCreateRVOBLayer.triggered.connect(self.runCreateRVOB )
        #
        # Create 3D Parameters Table
        self.actionCreate3DParams = QAction(self.tr("Create Table for 3D-array parameters (LPF)"), self.iface.mainWindow())
        self.actionCreate3DParams.setWhatsThis("Configuration for test plugin")
        self.actionCreate3DParams.setStatusTip("This is status tip")
        self.actionCreate3DParams.triggered.connect(self.run3DParamsTable )

        # Create 2D Parameters Table
        self.actionCreate2DParams = QAction(self.tr("Create Table for 2D Time-varying parameters (RCH, EVT)"), self.iface.mainWindow())
        self.actionCreate2DParams.setWhatsThis("Configuration for test plugin")
        self.actionCreate2DParams.setStatusTip("This is status tip")
        self.actionCreate2DParams.triggered.connect(self.run2DParamsTable )

        # Copy from vector to vector
        self.actionCopyFromVec = QAction(self.tr("Copy from &Vector layer"), self.iface.mainWindow())
        self.actionCopyFromVec.setWhatsThis("Configuration for test plugin")
        self.actionCopyFromVec.setStatusTip("This is status tip")
        self.actionCopyFromVec.triggered.connect(self.runCopyfromVecToVec)

        # Copy from raster to vector
        self.actionCopyFromRaster = QAction(self.tr("Copy from &Raster layer"), self.iface.mainWindow())
        self.actionCopyFromRaster.setWhatsThis("Configuration for test plugin")
        self.actionCopyFromRaster.setStatusTip("This is status tip")
        self.actionCopyFromRaster.triggered.connect(self.runCopyfromRasterToVec)

        # Merge Tool
        self.actionMergeTool = QAction(self.tr("Merge SpatiaLite layers"), self.iface.mainWindow())
        self.actionMergeTool.setWhatsThis("Configuration for test plugin")
        self.actionMergeTool.setStatusTip("This is status tip")
        self.actionMergeTool.triggered.connect(self.runMergeTool)

        # Add a stress period
        self.actionStressPeriod = QAction(self.tr("Add Stress Period"), self.iface.mainWindow())
        self.actionStressPeriod.setWhatsThis("Configuration for test plugin")
        self.actionStressPeriod.setStatusTip("This is status tip")
        self.actionStressPeriod.triggered.connect(self.runAddStressPeriod)

        # Update the working directory
        self.actionUpdateWd = QAction(self.tr("Update Working Directory"), self.iface.mainWindow())
        self.actionUpdateWd.setWhatsThis("Configuration for test plugin")
        self.actionUpdateWd.setStatusTip("This is status tip")
        self.actionUpdateWd.triggered.connect(self.runUpdateWd)


        # Model Builder
        self.actionModelBuilder = QAction(self.tr("Run Model"), self.iface.mainWindow())
        self.actionModelBuilder.setWhatsThis("Configuration for test plugin")
        self.actionModelBuilder.setStatusTip("This is status tip")
        self.actionModelBuilder.triggered.connect(self.runModelBuilder)

        # Model Output
        self.actionModelOutput = QAction(self.tr("View Model Output"), self.iface.mainWindow())
        self.actionModelOutput.setWhatsThis("Configuration for test plugin")
        self.actionModelOutput.setStatusTip("This is status tip")
        self.actionModelOutput.triggered.connect(self.runOutputView)

        # Cross Section
        self.actionCrossSection = QAction(self.tr("View a Cross Section"), self.iface.mainWindow())
        self.actionCrossSection.setWhatsThis("Configuration for test plugin")
        self.actionCrossSection.setStatusTip("This is status tip")
        self.actionCrossSection.triggered.connect(self.runCrossView)

        # # Model Budget
        self.actionBudget = QAction(self.tr("View Model Volumetric Budget"), self.iface.mainWindow())
        self.actionBudget.setWhatsThis("Configuration for test plugin")
        self.actionBudget.setStatusTip("This is status tip")
        self.actionBudget.triggered.connect(self.runBudgetView)

        # Zone Budget
        self.actionZoneBudget = QAction(self.tr("Run Zone Budget"), self.iface.mainWindow())
        self.actionZoneBudget.setWhatsThis("Configuration for test plugin")
        self.actionZoneBudget.setStatusTip("This is status tip")
        self.actionZoneBudget.triggered.connect(self.runZoneBudget)

        # Model Fit for Calibration
        self.actionModelFit = QAction(self.tr("Plot Model Fit"), self.iface.mainWindow())
        self.actionModelFit.setWhatsThis("Configuration for test plugin")
        self.actionModelFit.setStatusTip("This is status tip")
        self.actionModelFit.triggered.connect(self.runSimpleCalibrationPlot)


        # Plot FarmProcess Budgets
        self.actionPlotFmpBudget = QAction("Plot Budgets for Water Units", self.iface.mainWindow())
        self.actionPlotFmpBudget.setWhatsThis("Plot the results of a Water Management model")
        self.actionPlotFmpBudget.setStatusTip("This is status tip")
        self.actionPlotFmpBudget.triggered.connect(self.runPlotFmp)

	    # Plot Calibration
        # Temporaru solution  (iac)
        if runPlotLoaded:
            self.actionPlotCalibration = QAction("Plot Calibration Results", self.iface.mainWindow())
            self.actionPlotCalibration.setWhatsThis("Plot the results of a finished calibration")
            self.actionPlotCalibration.setStatusTip("This is status tip")
            self.actionPlotCalibration.triggered.connect(self.runPlotCalibration)

        self.actionCreatePartikelTrack = QAction("Particle Tracking (MODPATH)", self.iface.mainWindow())
        self.actionCreatePartikelTrack.setWhatsThis("Create input files for modpath")
        self.actionCreatePartikelTrack.setStatusTip("This is status tip")
        self.actionCreatePartikelTrack.triggered.connect(self.runCreatePartikelTracking)

        # SWI results Output
        self.actionSWIOutput = QAction(self.tr("View SWI Output"), self.iface.mainWindow())
        self.actionSWIOutput.setWhatsThis("Show results of SWI2 Package")
        self.actionSWIOutput.setStatusTip("This is status tip")
        self.actionSWIOutput.triggered.connect(self.runViewSWI)

        # Database sources (sub-menus)
        # Regione Toscana
        self.actionDataSourcesRT = QAction(QIcon(self.getIconPath("RT.png")), self.tr("Regione Toscana"), self.iface.mainWindow())
        self.actionDataSourcesRT.triggered.connect(self.RT)
        # GeoBasi
        self.actionDataSourcesGB = QAction(QIcon(self.getIconPath("RT.png")), self.tr("GeoBasi"), self.iface.mainWindow())
        self.actionDataSourcesGB.triggered.connect(self.GB)

        # About
        self.actionAbout = QAction(QIcon(self.getIconPath("about.png")), self.tr("About"), self.iface.mainWindow())
        self.actionAbout.setWhatsThis("Configuration for test plugin")
        self.actionAbout.setStatusTip("This is status tip")
        self.actionAbout.triggered.connect(self.runAbout)

        # -----
        # needed by akvaGIS
        # Add Sub-menu akvaGIS to main Menu, and relatives Actions

        self.menu.akvaGIS = QMenu(QCoreApplication.translate("akvaGIS", "Data-Preprocessing (akvaGIS)"))
        self.menu.addMenu(self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_CreateDB,
            parent=self.iface.mainWindow(),
            enabled=True, submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_OpenDB,
            parent=self.iface.mainWindow(),
            enabled=True, submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_CloseDB,
            parent=self.iface.mainWindow(),submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_ManageDB,
            parent=self.iface.mainWindow(),submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_QuerySamples,
            parent=self.iface.mainWindow(),submenu = self.menu.akvaGIS)
##
##        Not necessary now: all features are launched from Sub-menu
##        self.toolbar.addSeparator()
##
        self.add_feature(
            feature=self.m_Piper,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_SAR,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_SBD,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_stiffPlot,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_chemTimePlot,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_IBalance,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.toolbar.addSeparator()

        self.add_feature(
            feature=self.m_Map,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_MapNormative,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_StiffMap,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)
##
##        self.toolbar.addSeparator()
##
        self.add_feature(
            feature=self.m_EasyQuim,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_ExcelMix,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_StatQuimet,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

##        self.toolbar.addSeparator()

        self.add_feature(
            feature=self.m_HydroSamples,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_hydroTimePlot,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_HydroMap,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        self.add_feature(
            feature=self.m_HydroMapSurface,
            parent=self.iface.mainWindow(), submenu = self.menu.akvaGIS)

        # --- for akvaGIS until here


        #----------------------------
        # Add Sub-menu ModelSetUp to main Menu, and relatives Actions

        self.menu.ModelSetUp_menu = QMenu(QCoreApplication.translate("FREEWAT", "Model Setup"))
        self.menu.addMenu(self.menu.ModelSetUp_menu)

        self.menu.ModelSetUp_menu.addAction(self.actionCreateModel)
        self.menu.ModelSetUp_menu.addAction(self.actionCreateGrid)
        self.menu.ModelSetUp_menu.addAction(self.actionSetTMR)
        self.menu.ModelSetUp_menu.addAction(self.actionCreateModelLayer)
        self.menu.ModelSetUp_menu.addAction(self.actionStressPeriod)
        self.menu.ModelSetUp_menu.addAction(self.actionUpdateWd)
        self.menu.ModelSetUp_menu.addAction(self.actionModflowModel)

        # Add Sub-menu MDO to main Menu, and relatives Actions
        self.menu.createMDO_menu = QMenu(QCoreApplication.translate("FREEWAT", "MODFLOW Boundary Conditions"))
        self.menu.addMenu(self.menu.createMDO_menu)

        self.menu.createMDO_menu.addAction(self.actionCreateCHDLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateWELlayer)
        self.menu.createMDO_menu.addAction(self.actionCreateMNWlayer)
        self.menu.createMDO_menu.addAction(self.actionCreateRCHLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateRIVLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateLAKLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateDRNLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateGHBLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateEVTLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateUZFLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateSFRLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateSMLLayer)
        self.menu.createMDO_menu.addAction(self.actionCreateZoneLayer)

        # Add Sub-menu SWI to main Menu, and relatives Actions
        self.menu.createSWI_menu = QMenu(QCoreApplication.translate("FREEWAT", "Seawater Intrusion"))
        self.menu.addMenu(self.menu.createSWI_menu)
        self.menu.createSWI_menu.addAction(self.actionCreateSWILayer)

        # Add Sub-menu Transport to main Menu, and relatives Actions
        self.menu.transport_menu = QMenu(QCoreApplication.translate("FREEWAT", self.tr("Solute Transport Process")))
        self.menu.addMenu(self.menu.transport_menu)

        self.menu.transport_menu.addAction(self.actionCreateTrTableLayer)
        self.menu.transport_menu.addAction(self.actionCreateTRNSPLayer)
        self.menu.transport_menu.addAction(self.actionCreateSSMLayer)
        self.menu.transport_menu.addAction(self.actionCreateRCTLayer)
        self.menu.transport_menu.addAction(self.actionCreateUZTLayer)
        self.menu.transport_menu.addAction(self.actionCreateSFTLayer)
        self.menu.transport_menu.addAction(self.actionCreateLKTLayer)
        self.menu.transport_menu.addAction(self.actionCreateUSBLayer)
        self.menu.transport_menu.addAction(self.actionRunUSB)

        # Add Sub-menu Water Management and Farm to main Menu, and relatives Actions
        self.menu.farmprocess_menu = QMenu(QCoreApplication.translate("FREEWAT", "Water Management and Crop Modeling (FARM PROCESS) "))
        self.menu.addMenu(self.menu.farmprocess_menu)

        self.menu.farmprocess_menu.addAction(self.actionCreateFarmID )
        self.menu.farmprocess_menu.addAction(self.actionCreateWaterUnits)
        self.menu.farmprocess_menu.addAction(self.actionLoadNRD)
        self.menu.farmprocess_menu.addAction(self.actionCreateFarmWells)
        self.menu.farmprocess_menu.addAction(self.actionLoadGWAllotments)
        self.menu.farmprocess_menu.addAction(self.actionCreateFarmPipeline)
        self.menu.farmprocess_menu.addAction(self.actionLoadSWAllotments)
        self.menu.farmprocess_menu.addAction(self.actionCreateFarmCrops)
        self.menu.farmprocess_menu.addAction(self.actionCreateSoilsTable)
        self.menu.farmprocess_menu.addAction(self.actionCreateCropTable)
        self.menu.farmprocess_menu.addAction(self.actionCropCoeffients)
        self.menu.farmprocess_menu.addAction(self.actionCreateSMLFarmLayer)
        self.menu.farmprocess_menu.addAction(self.actionClimateData)
        self.menu.farmprocess_menu.addAction(self.actionCropModule)
        self.menu.farmprocess_menu.addAction(self.actionCropModel)

        # Add Sub-menu Calibration/Sensitivity to main Menu, and relatives Actions
        self.menu.calibration = QMenu(QCoreApplication.translate("FREEWAT", ("Calibration/Sensitivity")))
        self.menu.addMenu(self.menu.calibration)

        self.menu.calibration.addAction(self.actionCreateInputFilesForOBS)
        self.menu.calibration.addAction(self.actionCreateHOBLayer)
        self.menu.calibration.addAction(self.actionCreateRVOBLayer)
        self.menu.calibration.addAction(self.actionCreate3DParams)
        self.menu.calibration.addAction(self.actionCreate2DParams)


        # Add Sub-menu TOOLS to main Menu, and relatives Actions
        self.menu.tools = QMenu(QCoreApplication.translate("FREEWAT", "Tools"))
        self.menu.addMenu(self.menu.tools)
        self.menu.tools.addAction(self.actionCopyFromVec)
        self.menu.tools.addAction(self.actionCopyFromRaster)
        self.menu.tools.addAction(self.actionMergeTool)

        # Add Sub-menu Database for some additional data source
        self.menu.database = QMenu(QCoreApplication.translate("FREEWAT", "DataBase"))
        self.menu.addMenu(self.menu.database)
        self.menu.database.addAction(self.actionDataSourcesRT)
        self.menu.database.addAction(self.actionDataSourcesGB)

        # Add Actions to Main Menu
        self.menu.addAction(self.actionProgramLocations)

        # Add Actions to Main Menu
        self.menu.addAction(self.actionModelBuilder)


        # Add Actions to PostProcessing Menu
        self.menu.output = QMenu(QCoreApplication.translate("FREEWAT", "Post-processing"))
        self.menu.addMenu(self.menu.output)
        self.menu.output.addAction(self.actionModelOutput)
        self.menu.output.addAction(self.actionCrossSection)
        self.menu.output.addAction(self.actionBudget)
        self.menu.output.addAction(self.actionZoneBudget)
        self.menu.output.addAction(self.actionModelFit)
        # Temporary solution: (iac)
        if runPlotLoaded:
            self.menu.output.addAction(self.actionPlotCalibration)

        self.menu.output.addAction(self.actionPlotFmpBudget)
        self.menu.output.addAction(self.actionCreatePartikelTrack)
        self.menu.output.addAction(self.actionSWIOutput)


		# Add OAT to Main Menu
        # --
        #try:
        if oatLoaded:
            oatInit.create_oat_menu(self)
        else:
            from PyQt4.Qt import QMessageBox
            messageOAT  = ''' To run OAT (Observation Analysis Tool) you need that
                        the following Python modules are installed:
                        Numpy Version 1.7.1 or later
                        Pandas Version 0.17.1 or later
                        Please, check your installation!

                        OAT will not be loaded, until you have fixed
                        this dependency issue
                        but you can use FREEWAT anyway ... '''
            QMessageBox.information(self.iface.mainWindow(), "Warning", messageOAT)
        # #except:
        #    pass
        # ------------------------

        # Add Actions to Main Menu
        self.menu.addAction(self.actionAbout)


        menuBar = self.iface.mainWindow().menuBar()
        menuBar.insertMenu(self.iface.firstRightStandardMenu().menuAction(), self.menu)



    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr(u'&Freewat'), action)
            self.iface.removeToolBarIcon(action)
        self.menu and self.menu.setParent(None)
        del self.menu

    def runCreateModel(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgMD = mdDialog.CreateModelDialog(self.iface)
        # show the dialog
        self.dlgMD.show()
        # Run the dialog event loop
        self.dlgMD.exec_()

    def runProgramLocations(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgPL = plDialog.ProgramLocationsDialog(self.iface)
        # show the dialog
        self.dlgPL.show()
        # Run the dialog event loop
        self.dlgPL.exec_()


    def runCreateGrid(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgGR = grDialog.QGridderDialog(self.iface)
        # show the dialog
        self.dlgGR.show()
        # Run the dialog event loop
        self.dlgGR.exec_()

    def runCreateModelLayer(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgML = mlDialog.CreateModelLayerDialog(self.iface)
        # show the dialog
        self.dlgML.show()
        # Run the dialog event loop
        self.dlgML.exec_()

    def runCreateCHD(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgCHD = chdDialog.CreateCHDLayerDialog(self.iface)
        # show the dialog
        self.dlgCHD.show()
        # Run the dialog event loop
        self.dlgCHD.exec_()

    def runCreateWEL(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgWEL = welDialog.CreateWELayerDialog(self.iface)
        # show the dialog
        self.dlgWEL.show()
        # Run the dialog event loop
        self.dlgWEL.exec_()

    def runCreateMNW(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgMNW = mnwDialog.CreateMNWLayerDialog(self.iface)
        # show the dialog
        self.dlgMNW.show()
        # Run the dialog event loop
        self.dlgMNW.exec_()

    def runCreateRCH(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgRCH = rchDialog.CreateRCHLayerDialog(self.iface)
        # show the dialog
        self.dlgRCH.show()
        # Run the dialog event loop
        self.dlgRCH.exec_()

    def runCreateRIV(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgRIV = rivDialog.CreateRIVLayerDialog(self.iface)
        # show the dialog
        self.dlgRIV.show()
        # Run the dialog event loop
        self.dlgRIV.exec_()

    def runCreateLAK(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgLAK = lakDialog.CreateLAKLayerDialog(self.iface)
        # show the dialog
        self.dlgLAK.show()
        # Run the dialog event loop
        self.dlgLAK.exec_()

    def runCreateDRN(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgDRN = drnDialog.CreateDRNLayerDialog(self.iface)
        # show the dialog
        self.dlgDRN.show()
        # Run the dialog event loop
        self.dlgDRN.exec_()

    def runCreateGHB(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgGHB = ghbDialog.CreateGHBLayerDialog(self.iface)
        # show the dialog
        self.dlgGHB.show()
        # Run the dialog event loop
        self.dlgGHB.exec_()

    def runCreateEVT(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgEVT = evtDialog.CreateEVTLayerDialog(self.iface)
        # show the dialog
        self.dlgEVT.show()
        # Run the dialog event loop
        self.dlgEVT.exec_()

    def runCreateUZF(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgUZF = uzfDialog.CreateUZFLayerDialog(self.iface)
        # show the dialog
        self.dlgUZF.show()
        # Run the dialog event loop
        self.dlgUZF.exec_()

    def runCreateSML(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSML = smlDialog.CreateSMLLayerDialog(self.iface)
        # show the dialog
        self.dlgSML.show()
        # Run the dialog event loop
        self.dlgSML.exec_()

    def runCreateTrTable(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgTrTB = ttableDialog.CreateTransportTable(self.iface)
        # show the dialog
        self.dlgTrTB.show()
        # Run the dialog event loop
        self.dlgTrTB.exec_()

    def runCreateTRNSP(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgTRNSP = trnspDialog.CreateTRNSPLayerDialog(self.iface)
        # show the dialog
        self.dlgTRNSP.show()
        # Run the dialog event loop
        self.dlgTRNSP.exec_()

    def runCreateSSM(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSSM = ssmDialog.CreateSSMLayerDialog(self.iface)
        # show the dialog
        self.dlgSSM.show()
        # Run the dialog event loop
        self.dlgSSM.exec_()

    def runCreateRCT(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgRCT = rctDialog.CreateReactionLayerDialog(self.iface)
        # show the dialog
        self.dlgRCT.show()
        # Run the dialog event loop
        self.dlgRCT.exec_()

    def runCreateSFR(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSFR = sfrDialog.CreateSFRLayerDialog(self.iface)
        # show the dialog
        self.dlgSFR.show()
        # Run the dialog event loop
        self.dlgSFR.exec_()

    def runCreateFarmID(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgFmpId = fmpidDialog.createFarmIdDialog(self.iface)
        # show the dialog
        self.dlgFmpId.show()
        # Run the dialog event loop
        self.dlgFmpId.exec_()

    def runDefineWaterUnits(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgWaterUnits = watersitesDialog.CreateWaterSitesTableDialog(self.iface)
        # show the dialog
        self.dlgWaterUnits.show()
        # Run the dialog event loop
        self.dlgWaterUnits.exec_()

    def runLoadGWAlots(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgGWalot = gwallotDialog.LoadGWAllotmentsDialog(self.iface)
        # show the dialog
        self.dlgGWalot.show()
        # Run the dialog event loop
        self.dlgGWalot.exec_()

    def runLoadSWAlots(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSWalot = swallotDialog.LoadSWAllotmentsDialog(self.iface)
        # show the dialog
        self.dlgSWalot.show()
        # Run the dialog event loop
        self.dlgSWalot.exec_()

    def runLoadNRDeliv(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgNRD = nrdelivDialog.LoadNRDeliveriesDialog(self.iface)
        # show the dialog
        self.dlgNRD.show()
        # Run the dialog event loop
        self.dlgNRD.exec_()

    def runCreateFarmWells(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgFmpWell = fmpwellDialog.CreateFarmWELayerDialog(self.iface)
        # show the dialog
        self.dlgFmpWell.show()
        # Run the dialog event loop
        self.dlgFmpWell.exec_()

    def runCreateFarmCrop(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgFmpCrop = cropDialog.createFarmCropSoilDialog(self.iface)
        # show the dialog
        self.dlgFmpCrop.show()
        # Run the dialog event loop
        self.dlgFmpCrop.exec_()

    def runCreateCropTable(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgCropTable = croptableDialog.CreateCropTableDialog(self.iface)
        # show the dialog
        self.dlgCropTable.show()
        # Run the dialog event loop
        self.dlgCropTable.exec_()

    def runCreateSoilsTable(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSoilsTable = soilstableDialog.CreateSoilsTableDialog(self.iface)
        # show the dialog
        self.dlgSoilsTable.show()
        # Run the dialog event loop
        self.dlgSoilsTable.exec_()

    def runCreateFarmPipeline(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgFmpPipeline = fmpipeDialog.createFarmPipelineDialog(self.iface)
        # show the dialog
        self.dlgFmpPipeline.show()
        # Run the dialog event loop
        self.dlgFmpPipeline.exec_()

    def runLoadClimateData(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgClimateData = climateDialog.LoadClimateDataDialog(self.iface)
        # show the dialog
        self.dlgClimateData.show()
        # Run the dialog event loop
        self.dlgClimateData.exec_()

    def runCropModule(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgCropModule = CropModuleDialog.createCropDialog(self.iface)
        # show the dialog
        self.dlgCropModule.show()
        # Run the dialog event loop
        self.dlgCropModule.exec_()

    def runCropModel(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgRunCropModule = CropRunDialog.runCropDialog(self.iface)
        # show the dialog
        self.dlgRunCropModule.show()
        # Run the dialog event loop
        self.dlgRunCropModule.exec_()

    def runLoadCropCoeffients(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgCropCoefficents = cropCoefficientDialog.LoadCropCoefficientsDialog(self.iface)
        # show the dialog
        self.dlgCropCoefficents.show()
        # Run the dialog event loop
        self.dlgCropCoefficents.exec_()

    def runAddStressPeriod(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgSP = spDialog.CreateAddSPDialog(self.iface)
        # show the dialog
        self.dlgSP.show()
        # Run the dialog event loop
        self.dlgSP.exec_()

    def runUpdateWd(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgWd = updateWd.UpdateWorkingDirectory(self.iface)
        # show the dialog
        self.dlgWd.show()
        # Run the dialog event loop
        self.dlgWd.exec_()

    def runCopyfromVecToVec(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgVector = copyVectorDialog.CopyFromVector(self.iface)
        # show the dialog
        self.dlgVector.show()
        # Run the dialog event loop
        self.dlgVector.exec_()

    def runCopyfromRasterToVec(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgRaster = copyRasterDialog.copyRasterToFields(self.iface)
        # show the dialog
        self.dlgRaster.show()
        # Run the dialog event loop
        self.dlgRaster.exec_()

    def runMergeTool(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgMerge = mergeDialog.MergeToolDialog(self.iface)
        # show the dialog
        self.dlgMerge.show()
        # Run the dialog event loop
        self.dlgMerge.exec_()

    def runModelBuilder(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgMB = mbDialog.ModelBuilderDialog(self.iface)
        # show the dialog
        #self.dlgMB.show()
        # Run the dialog event loop
        self.dlgMB.exec_()

    def runOutputView(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgOUT = outDialog.viewOutputDialog(self.iface)
        # show the dialog
        self.dlgOUT.show()
        # Run the dialog event loop
        self.dlgOUT.exec_()

    def runCrossView(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgCross = csDialog.viewOutputDialog(self.iface)
        # Run the dialog event loop
        self.dlgCross.exec_()

    def runBudgetView(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = budgetDialog.viewBudgetDialog(self.iface)
        # show the dialog
        # dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def runCreateZone(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgZON = zoneDialog.CreateZoneLayerDialog(self.iface)
        # show the dialog
        self.dlgZON.show()
        # Run the dialog event loop
        self.dlgZON.exec_()

    def runZoneBudget(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgZBUD = zbudDialog.runZoneBudgetDialog(self.iface)
        # show the dialog
        self.dlgZBUD.show()
        # Run the dialog event loop
        self.dlgZBUD.exec_()

    def runPlotCalibration(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        # Temporary solution (iac)
        if runPlotLoaded:
            self.dlgPC = pcDialog.RunPlotCalibrationDialog(self.iface)
            # show the dialog
            self.dlgPC.show()
            # Run the dialog event loop
            self.dlgPC.exec_()

    def runSimpleCalibrationPlot(self):
        self.dlgSCP = scpDialog.SimplePlotCalibration(self.iface)
        # show the dialog
        self.dlgSCP.show()
        # Run the dialog event loop
        self.dlgSCP.exec_()

    def runCreatePartikelTracking(self):
        self.dlgPT = ptDialog.createPartikelTracking(self.iface)
        # show the dialog
        self.dlgPT.show()
        # Run the dialog event loop
        self.dlgPT.exec_()

    def runPlotFmp(self):
        self.dlgPFMP = fmpPlotDialog.plotWaterUnitBudgetDialog(self.iface)
        # show the dialog
        self.dlgPFMP.show()
        # Run the dialog event loop
        self.dlgPFMP.exec_()

    def runCreateInputFilesForOBS(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = OBSfiles.CreateInputFilesForOBS(self.iface)
        # Run the dialog event loop
        dlg.exec_()


    def runCreateHOB(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgHOB = hobDialog.CreateHOBLayerDialog(self.iface)
        # show the dialog
        self.dlgHOB.show()
        # Run the dialog event loop
        self.dlgHOB.exec_()

    def runCreateRVOB(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlgRVOB = rvobDialog.CreateROVBLayerDialog(self.iface)
        # show the dialog
        dlgRVOB.show()
        # Run the dialog event loop
        dlgRVOB.exec_()

    def run3DParamsTable(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg3Dparams = param3DDialog.CreateParams3DLayerDialog(self.iface)
        # show the dialog
        dlg3Dparams.show()
        # Run the dialog event loop
        dlg3Dparams.exec_()

    def run2DParamsTable(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg2Dparams = param2DDialog.CreateParams2DLayerDialog(self.iface)
        # show the dialog
        dlg2Dparams.show()
        # Run the dialog event loop
        dlg2Dparams.exec_()

    def runCreateUZT(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = uztDialog.CreateUZTLayerDialog(self.iface)
        # Run the dialog event loop
        dlg.exec_()

    def runCreateSFT(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = sftDialog.CreateSFTLayerDialog(self.iface)
        # Run the dialog event loop
        dlg.exec_()

    def runCreateLKT(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = lktDialog.CreateLKTLayerDialog(self.iface)
        # Run the dialog event loop
        dlg.exec_()

    def runCreateUSB(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = usbDialog.CreateUSBLayerDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def runUSB(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = runUsbDialog.ComputeUSBDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def runTMR(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = tmrDialog.CreateTMRFileDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def RT(self):
        RT = 'http://dati.toscana.it/dataset'
        webbrowser.open(RT)

    def GB(self):
        GB = 'http://www506.regione.toscana.it/geobasi/index.html'
        webbrowser.open(GB)

    def runAbout(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        self.dlgAbt = aboutDialog.AboutDialog(self.iface)
        # show the dialog
        self.dlgAbt.show()
        # Run the dialog event loop
        self.dlgAbt.exec_()

    def runImportModflow(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = importModflowDialog.ImportModflowModelDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def runCreateSWI(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = swiDialog.CreateSwiLayerDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()

    def runViewSWI(self):
        """Run method that performs all the real work"""
        # Create the dialog (after translation) and keep reference
        dlg = viewSWIDialog.viewOutputDialog(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()
